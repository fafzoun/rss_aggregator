FROM python:3.7

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /rss_scraper

# Set the working directory to /music_service
WORKDIR /rss_scraper

# Copy the current directory contents into the container at /music_service
ADD . /rss_scraper/

# Install Pipenv
RUN pip install pipenv

# Install requirements
RUN pipenv install --system --deploy --ignore-pipfile
