from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from rss_scraper.apps.membership.models import User
from rss_scraper.apps.membership.serializers import UserSerializer


class UserViewSet(mixins.CreateModelMixin,
                  GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
