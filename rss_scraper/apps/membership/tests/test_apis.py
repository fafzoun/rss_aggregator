from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIRequestFactory

from rss_scraper.apps.membership.apis import UserViewSet
from rss_scraper.apps.membership.models import User

client = Client()


class UserTest(TestCase):
    def test_create_authenticate_user(self):
        # Test User Creation
        create_request = APIRequestFactory().post("", data={
            "username": "doe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "john@doe.com",
            "password": "!@#QWEASD",
        })

        view = UserViewSet.as_view({'post': 'create'})

        create_response = view(create_request)
        self.assertEqual(create_response.status_code, status.HTTP_201_CREATED)

        # Test User Authentication
        john = User.objects.get(id=create_response.data.get('id'))

        # Test Inactive User Authentication
        authenticate_response = client.post(reverse('authenticate'), data={
            "username": "doe",
            "password": "!@#QWEASD"
        })
        self.assertEqual(authenticate_response.status_code, status.HTTP_400_BAD_REQUEST)

        john.is_active = True
        john.save()

        # Test Active User Authentication
        authenticate_response = client.post(reverse('authenticate'), data={
            "username": "doe",
            "password": "!@#QWEASD"
        })
        self.assertEqual(authenticate_response.status_code, status.HTTP_200_OK)
