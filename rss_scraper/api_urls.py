from django.urls import path, include

from rss_scraper.apps.membership import api_urls as membership_urls
from rss_scraper.apps.rss_feed import api_urls as rss_feed_urls

urlpatterns = [
    path('users/', include(membership_urls)),
    path('feeds/', include(rss_feed_urls)),
]
