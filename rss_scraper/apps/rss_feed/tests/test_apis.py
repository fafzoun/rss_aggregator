from django.test import TestCase, Client
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from rss_scraper.apps.membership.models import User
from rss_scraper.apps.rss_feed.apis import UserFeedViewSet, UserEntryViewSet
from rss_scraper.apps.rss_feed.entry_serializers import UserEntrySerializer
from rss_scraper.apps.rss_feed.models import Feed, UserFeed, UserEntry
from rss_scraper.apps.rss_feed.serializers import UserFeedSerializer

client = Client()


class FeedTest(TestCase):
    def setUp(self):
        self.feed1 = Feed.objects.create(
            xml_url="https://www.nasa.gov/rss/dyn/breaking_news.rss"
        )
        self.feed2 = Feed.objects.create(
            xml_url="https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss"
        )
        self.feed3 = Feed.objects.create(
            xml_url="https://www.nasa.gov/rss/dyn/educationnews.rss"
        )

        self.john = User.objects.create(
            username="doe",
            first_name="John",
            last_name="Doe",
            email="john@doe.com",
            password="!@#QWEASD",
            is_active=True
        )
        self.john.set_password("!@#QWEASD")
        self.john.save()

        self.jane = User.objects.create(
            username="jane",
            first_name="Jane",
            last_name="Doe",
            email="jane@doe.com",
            password="!@#QWEASD",
            is_active=True
        )
        self.jane.set_password("!@#QWEASD")
        self.jane.save()

        self.john1 = UserFeed.objects.create(
            feed=self.feed1,
            user=self.john
        )
        self.john2 = UserFeed.objects.create(
            feed=self.feed2,
            user=self.john
        )
        self.jane3 = UserFeed.objects.create(
            feed=self.feed3,
            user=self.jane
        )

    def test_get_all_feeds(self):
        request = APIRequestFactory().get("")

        view = UserFeedViewSet.as_view({'get': 'list'})

        # Unauthorized Request
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate John
        force_authenticate(request, self.john)

        # John Response Status Code
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # John Response Data
        john_feeds = UserFeedSerializer(UserFeed.objects.filter(user=self.john), many=True)
        self.assertEqual(response.data, john_feeds.data)

        # Authenticate Jane
        force_authenticate(request, self.jane)

        # Jane Response Status Code
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Jane Response Data
        jane_feeds = UserFeedSerializer(UserFeed.objects.filter(user=self.jane), many=True)
        self.assertEqual(response.data, jane_feeds.data)

    def test_get_single_feed(self):
        request = APIRequestFactory().get("")

        view = UserFeedViewSet.as_view({'get': 'retrieve'})

        # Unauthorized Request
        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate John
        force_authenticate(request, self.john)

        # Test Wrong PK
        response = view(request, pk=122)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Test Right PK
        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, UserFeedSerializer(self.john1).data)

        # Authenticate Jane To Get John's Feed Detail
        force_authenticate(request, self.jane)

        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_feed(self):
        # Valid XML URL
        request = APIRequestFactory().post("", data={
            "feed": "https://www.cnet.com/rss/news/",
        })

        view = UserFeedViewSet.as_view({'post': 'create'})

        # UnAuthorized Request
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate Jane
        force_authenticate(request, self.jane)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Invalid XML URL
        request = APIRequestFactory().post("", data={
            "feed": "https://docs.djangoproject.com/en/3.0/",
        })
        view = UserFeedViewSet.as_view({'post': 'create'})

        # Authenticate Jane
        force_authenticate(request, self.jane)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_feed(self):
        request = APIRequestFactory().put("", data={
            "feed": "https://www.nasa.gov/rss/dyn/breaking_news.rss",
            "title": "NASA Breaking News",
            "description": "",
            "link": "https://www.nasa.gov/news/releases/latest/index.html",
            "bookmark": True
        })

        view = UserFeedViewSet.as_view({'put': 'update'})

        # Unauthorized Request
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate John
        force_authenticate(request, self.john)

        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Authenticate Jane To Change John's Feed Detail
        force_authenticate(request, self.jane)

        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_feed(self):
        request = APIRequestFactory().delete("")

        view = UserFeedViewSet.as_view({'delete': 'destroy'})

        # Unauthorized Request
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate Jane To Delete John's Feed Detail
        force_authenticate(request, self.jane)

        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Authenticate John
        force_authenticate(request, self.john)

        response = view(request, pk=self.john1.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_get_all_entries(self):
        request = APIRequestFactory().get("")

        view = UserEntryViewSet.as_view({'get': 'list'})

        # Unauthorized Request
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Authenticate John
        force_authenticate(request, self.john)

        # John Response Status Code
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # John Response Data
        john_entries = UserEntrySerializer(UserEntry.objects.filter(user=self.john), many=True)
        self.assertEqual(response.data, john_entries.data)

        # Create New Feed To Have Some Data For Jane
        request = APIRequestFactory().post("", data={
            "feed": "https://www.nasa.gov/rss/dyn/breaking_news.rss",
        })

        view = UserFeedViewSet.as_view({'post': 'create'})

        force_authenticate(request, self.jane)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check Jane Entries
        request = APIRequestFactory().get("")

        view = UserEntryViewSet.as_view({'get': 'list'})

        # Authenticate Jane
        force_authenticate(request, self.jane)

        # Jane Response Status Code
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Jane Response Data
        jane_feeds = UserEntrySerializer(UserEntry.objects.filter(user=self.jane), many=True)
        self.assertEqual(response.data, jane_feeds.data)

    def test_get_single_entry(self):
        # Create New Feed To Have Some Data For Jane
        request = APIRequestFactory().post("", data={
            "feed": "https://www.nasa.gov/rss/dyn/breaking_news.rss",
        })

        view = UserFeedViewSet.as_view({'post': 'create'})

        force_authenticate(request, self.jane)

        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Check Entry Detail
        entry = UserEntry.objects.filter(user=self.jane).first()

        request = APIRequestFactory().get("")
        view = UserEntryViewSet.as_view({'get': 'retrieve'})

        # Authenticate Jane
        force_authenticate(request, self.jane)

        response = view(request, pk=entry.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Authenticate John To Get Jane's Entry Detail
        force_authenticate(request, self.john)

        response = view(request, pk=entry.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
