from django.apps import AppConfig


class MembershipConfig(AppConfig):
    name = 'rss_scraper.apps.membership'
