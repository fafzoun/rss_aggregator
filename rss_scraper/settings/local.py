class General:
    DEBUG = False
    ALLOWED_HOSTS = ['*']


class Redis:
    HOST = "redis"
    PORT = "6379"
