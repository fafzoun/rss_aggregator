from rest_framework import serializers, exceptions

from rss_scraper.apps.rss_feed.models import Entry, UserEntry


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = '__all__'

    def validate(self, attrs):
        guid = attrs.get('guid', None)
        title = attrs.get('title', None)
        pubDate = attrs.get('pubDate', None)
        if guid:
            if Entry.objects.filter(guid=guid):
                raise exceptions.NotAcceptable
        if pubDate:
            if Entry.objects.filter(pubDate=pubDate):
                raise exceptions.NotAcceptable
        if title:
            if Entry.objects.filter(title=title):
                raise exceptions.NotAcceptable
        return super().validate(attrs)

    def create(self, validated_data):
        entry = super().create(validated_data)

        for user_feed in entry.feed.user_feed.all():
            user_entry_serializer = UserEntrySerializer(
                # user=user_feed.user,
                data={
                    'entry': entry.id
                },
                user=user_feed.user
            )
            user_entry_serializer.is_valid(raise_exception=True)
            user_entry_serializer.create(user_entry_serializer.validated_data)

        return entry

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        return representation


class UserEntrySerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = UserEntry
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def validate(self, attrs):
        attrs.update({
            'user': self.user
        })
        return attrs

    def create(self, validated_data):
        user_entry = super().create(validated_data)

        return user_entry

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)

        return instance

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['entry'] = EntrySerializer(instance.entry).data

        return representation
