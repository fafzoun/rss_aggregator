from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import ObtainJSONWebToken, RefreshJSONWebToken

from . import apis

router = DefaultRouter()
router.register(r'', apis.UserViewSet)

urlpatterns = [
    path('authenticate/', ObtainJSONWebToken.as_view(), name='authenticate'),
    path('authenticate/refresh/', RefreshJSONWebToken.as_view()),
    path('', include(router.urls)),
]
