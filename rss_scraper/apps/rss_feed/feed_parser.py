import datetime
import logging
import xml.etree.ElementTree as ET

import requests
from django.conf import settings
from rest_framework import exceptions

from rss_scraper.apps.rss_feed.entry_serializers import EntrySerializer

logger = logging.getLogger(__name__)


def poll_feed(feed):
    now = datetime.datetime.now()

    try:
        feed_response = requests.get(feed.xml_url)
    except Exception as e:
        logger.error(e)
        raise exceptions.ValidationError({"feed": "RSS Feed URL is not responding."})

    try:
        root = ET.fromstring(feed_response.content.decode("utf-8"))
        channel = root[0]
    except Exception as e:
        logger.error(e)
        raise exceptions.ValidationError({"feed": "RSS Feed URL is not valid."})

    feed.last_polled_time = now
    feed.next_poll_time = now + datetime.timedelta(minutes=settings.APPS['FEED']['POLL_INTERVAL'])
    feed.failed_polls = 0
    feed.save()

    items = []
    for item in channel.findall('item'):
        item_data = {}
        for attr in item:
            item_data[attr.tag] = attr.text
        items.append(item_data)

    for item_data in items:
        item_data.update({
            'feed': feed.id
        })

        entry_serializer = EntrySerializer(data=item_data)
        try:
            entry_serializer.is_valid(raise_exception=True)
        except exceptions.NotAcceptable:
            break
        entry_serializer.create(entry_serializer.validated_data)
