from django.apps import AppConfig


class RssFeedConfig(AppConfig):
    name = 'rss_scraper.apps.rss_feed'
