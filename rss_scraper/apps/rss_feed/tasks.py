import datetime

from celery.decorators import periodic_task
from celery.schedules import timedelta
from celery.utils.log import get_task_logger
from django.conf import settings

from rss_scraper.apps.rss_feed.feed_parser import poll_feed
from rss_scraper.apps.rss_feed.models import Feed

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(timedelta(minutes=settings.APPS['FEED']['POLL_INTERVAL'])),
    name="poll_feeds",
    ignore_result=True
)
def poll_feeds():
    now = datetime.datetime.now()
    feeds = Feed.objects.filter(next_poll_time__lte=now, user_feed__isnull=False, valid=True)
    for feed in feeds:
        try:
            poll_feed(feed)
        except Exception as e:
            logger.error(e)
            failed_polls = feed.failed_polls
            feed.failed_polls = failed_polls + 1
            time_delta = settings.APPS['FEED']['POLL_INTERVAL'] * (failed_polls + 1)
            feed.next_poll_time = feed.next_poll_time + datetime.timedelta(minutes=time_delta)
            feed.save()
