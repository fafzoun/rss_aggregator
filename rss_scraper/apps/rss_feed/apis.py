from rest_framework import mixins, permissions
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from rss_scraper.apps.rss_feed.entry_serializers import UserEntrySerializer
from rss_scraper.apps.rss_feed.models import UserFeed, UserEntry
from rss_scraper.apps.rss_feed.serializers import UserFeedSerializer


class UserFeedViewSet(mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.CreateModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin,
                      GenericViewSet):
    serializer_class = UserFeedSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        filters = {
            'user': self.request.user
        }
        bookmark_flag = self.request.GET.get('bookmark', None)
        if bookmark_flag:
            filters.update({
                'bookmark': bool(int(bookmark_flag))
            })

        return UserFeed.objects.filter(**filters)


class UserEntryViewSet(mixins.ListModelMixin,
                       mixins.RetrieveModelMixin,
                       GenericViewSet):
    serializer_class = UserEntrySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        filters = {
            'user': self.request.user
        }
        read_flag = self.request.GET.get('read', None)
        if read_flag:
            filters.update({
                'read': bool(int(read_flag))
            })

        return UserEntry.objects.filter(**filters)

    def retrieve(self, request, *args, **kwargs):
        entry = self.get_object()
        entry.read = True
        entry.save()

        return Response(data=UserEntrySerializer(entry).data)
