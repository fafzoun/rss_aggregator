from django.conf import settings
from django.db import models
from django.utils import timezone

from rss_scraper.apps.membership.models import User


class Feed(models.Model):
    xml_url = models.CharField(max_length=255, unique=True)
    create_time = models.DateTimeField(default=timezone.now)
    valid = models.BooleanField(default=True)
    last_polled_time = models.DateTimeField(blank=True, null=True)
    next_poll_time = models.DateTimeField(blank=True, null=True)
    failed_polls = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.xml_url

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.failed_polls >= settings.APPS['FEED']['MAX_FAILED_POLL_COUNT']:
            self.valid = False
        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)


class Entry(models.Model):
    class Meta:
        verbose_name_plural = 'Entries'
        ordering = ['-create_time']

    feed = models.ForeignKey(Feed, on_delete=models.CASCADE, related_name='entry')
    title = models.CharField(max_length=2000, blank=True, null=True)
    link = models.CharField(max_length=2000)
    description = models.TextField(blank=True, null=True)
    guid = models.CharField(max_length=2000, null=True, blank=True)
    pubDate = models.CharField(max_length=128, null=True, blank=True)
    create_time = models.DateTimeField(default=timezone.now)
    update_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title or self.link


class UserFeed(models.Model):
    class Meta:
        verbose_name = "User Feed"
        verbose_name_plural = "User Feeds"

        constraints = [
            models.UniqueConstraint(fields=['user', 'feed'], name='feed_unique'),
        ]

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_feed')
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE, related_name='user_feed')
    title = models.CharField(max_length=2000, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=2000, blank=True, null=True)
    bookmark = models.BooleanField(default=False)
    create_time = models.DateTimeField(default=timezone.now)
    update_time = models.DateTimeField(auto_now=True)

    @property
    def xml_url(self):
        return self.feed.xml_url

    @property
    def username(self):
        return self.user.username

    def __str__(self):
        return '{} {}'.format(self.user, self.feed)

    def delete(self, using=None, keep_parents=False):
        # noinspection PyBroadException
        try:
            UserEntry.objects.filter(user=self.user, entry__feed=self.feed).delete()
        except Exception:
            pass

        super().delete(using=using, keep_parents=keep_parents)


class UserEntry(models.Model):
    class Meta:
        verbose_name = "User Entry"
        verbose_name_plural = "User Entries"
        ordering = ['-create_time']

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_entry')
    entry = models.ForeignKey(Entry, on_delete=models.CASCADE, related_name='user_entry')
    read = models.BooleanField(default=False)
    create_time = models.DateTimeField(default=timezone.now)
    update_time = models.DateTimeField(auto_now=True)

    @property
    def link(self):
        return self.entry.link

    @property
    def username(self):
        return self.user.username

    def __str__(self):
        return '{} {}'.format(self.user, self.entry)
