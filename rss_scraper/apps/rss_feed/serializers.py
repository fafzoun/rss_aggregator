from django.db import transaction
from rest_framework import serializers, exceptions

from rss_scraper.apps.rss_feed.feed_parser import poll_feed
from rss_scraper.apps.rss_feed.models import Feed, UserFeed


class UserFeedSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    feed = serializers.CharField(required=True, allow_blank=False)

    class Meta:
        model = UserFeed
        fields = '__all__'

    def validate(self, attrs):
        feed, created = Feed.objects.get_or_create(xml_url=attrs.pop('feed'))
        user = self.context['request'].user

        if not self.instance:
            try:
                UserFeed.objects.get(
                    feed=feed,
                    user=user
                )
                raise exceptions.ValidationError({"feed": "You have already followed this feed."})
            except UserFeed.DoesNotExist:
                pass

        attrs.update({
            'feed': feed,
            'user': user
        })
        return attrs

    def create(self, validated_data):
        user_feed = super().create(validated_data)

        return user_feed

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)

        return instance

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['feed'] = instance.feed.xml_url if instance.feed else None

        return representation

    @transaction.atomic
    def save(self, **kwargs):
        user_feed = super().save(**kwargs)

        poll_feed(user_feed.feed)
