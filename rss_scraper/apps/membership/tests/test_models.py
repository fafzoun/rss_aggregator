from django.test import TestCase

from ..models import User


class UserTest(TestCase):
    def setUp(self):
        User.objects.create(
            username="john",
            first_name="John",
            last_name="Doe",
            email="john@doe.com",
            password="!@#QWEASD"
        )
        User.objects.create(
            username="jane",
            first_name="Jane",
            last_name="Doe",
            email="jane@doe.com",
            password="!@#QWEASD"
        )

    def test_user_full_name(self):
        john = User.objects.get(username="john")
        jane = User.objects.get(username="jane")
        self.assertEqual(john.get_full_name(), "John Doe")
        self.assertEqual(jane.get_full_name(), "Jane Doe")
