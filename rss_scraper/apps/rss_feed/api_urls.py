from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import apis

router = DefaultRouter()
router.register(r'entries', apis.UserEntryViewSet, basename='UserEntry')
router.register(r'', apis.UserFeedViewSet, basename='UserFeed')

urlpatterns = [
    path('', include(router.urls)),
]
