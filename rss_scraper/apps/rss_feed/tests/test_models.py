from django.test import TestCase

from rss_scraper.apps.membership.models import User
from ..models import Feed, Entry, UserFeed, UserEntry


class FeedTest(TestCase):
    def setUp(self):
        feed = Feed.objects.create(
            xml_url="https://www.nasa.gov/rss/dyn/breaking_news.rss"
        )

        entry = Entry.objects.create(
            feed=feed,
            title="Next Space Station Crew Takes a Break From Training",
            link="http://www.nasa.gov/image-feature/next-space-station-crew-takes-a-break-from-training",
            description="Expedition 63 crewmembers Chris Cassidy of NASA (left) and Anatoly Ivanishin (center) and " \
                        "Ivan Vagner of Roscosmos (right) pose for pictures in front of a Soyuz trainer.",
            guid="http://www.nasa.gov/image-feature/next-space-station-crew-takes-a-break-from-training",
            pubDate="Mon, 16 Mar 2020 07:44 EDT"
        )

        user = User.objects.create(
            username="john",
            first_name="John",
            last_name="Doe",
            email="john@doe.com",
            password="!@#QWEASD"
        )

        UserFeed.objects.create(
            user=user,
            feed=feed
        )

        UserEntry.objects.create(
            user=user,
            entry=entry
        )

    def test_details(self):
        feed = Feed.objects.get(xml_url="https://www.nasa.gov/rss/dyn/breaking_news.rss")
        self.assertEqual(feed.valid, True)
        self.assertEqual(feed.failed_polls, 0)

        entry = Entry.objects.get(feed=feed, title="Next Space Station Crew Takes a Break From Training")
        self.assertEqual(entry.link,
                         "http://www.nasa.gov/image-feature/next-space-station-crew-takes-a-break-from-training")
        self.assertEqual(entry.guid,
                         "http://www.nasa.gov/image-feature/next-space-station-crew-takes-a-break-from-training")

        john = User.objects.get(username="john")
        self.assertEqual(john.get_full_name(), "John Doe")

        user_feed = UserFeed.objects.get(user=john, feed=feed)
        self.assertEqual(user_feed.xml_url, "https://www.nasa.gov/rss/dyn/breaking_news.rss")
        self.assertEqual(user_feed.username, "john")

        user_entry = UserEntry.objects.get(
            user=john,
            entry=entry
        )
        self.assertEqual(user_entry.link,
                         "http://www.nasa.gov/image-feature/next-space-station-crew-takes-a-break-from-training")
        self.assertEqual(user_entry.username, "john")
