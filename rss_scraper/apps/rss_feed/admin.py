from django.contrib import admin

from .models import Feed, Entry, UserFeed, UserEntry

admin.site.register(Feed)
admin.site.register(Entry)
admin.site.register(UserFeed)
admin.site.register(UserEntry)
